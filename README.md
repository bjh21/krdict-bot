# krdict-bot

This is not actually a bot, but it is a potential collection of tools
for working with the image repository of the Basic Korean Dictionary.

See https://commons.wikimedia.org/wiki/Category:Media_from_the_Basic_Korean_Dictionary for slightly more information.
